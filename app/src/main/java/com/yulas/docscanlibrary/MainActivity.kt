package com.yulas.docscanlibrary

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.webkit.PermissionRequest
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yulas.docscanlibrary.databinding.ActivityMainBinding
import com.yulas.docscannerlibrary.Constants
import com.yulas.docscannerlibrary.ScanActivity
import timber.log.Timber
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val requestCode = 99
    private var imageUri: Uri? = null

    private val permissions =
            listOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        requestPermission(permissions, {}, {})
        init()
    }

    private fun init() {
        with(binding) {
            scanButton.setOnClickListener {
                tvResult.isGone = !tvResult.isGone
                scannedImage.isGone = !scannedImage.isGone
            }
            cameraButton.setOnClickListener {
                Timber.d("Timber MainActivity cameraButton")
                startScan((Constants.OPEN_CAMERA))
            }
            mediaButton.setOnClickListener {
                Timber.d("Timber MainActivity mediaButton")
                startScan((Constants.OPEN_MEDIA))
            }
            reverseButton.setOnClickListener {
                tvResult.isGone = !tvResult.isGone
                scannedImage.isGone = !scannedImage.isGone
            }
        }
    }

    private fun startScan(preference: Int) {
        val intent = Intent(this, ScanActivity::class.java)
        intent.putExtra(Constants.OPEN_INTENT_PREFERENCE, preference)
        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(
            requestCode: Int,
            resultCode: Int,
            data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == this.requestCode && resultCode == RESULT_OK) {
            val uri = data?.extras!!.getParcelable<Uri>(Constants.SCANNED_RESULT)
            imageUri = uri
            var bitmap: Bitmap? = null
            try {
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                processImage()
                contentResolver.delete(uri!!, null, null)
                binding.scannedImage.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun processImage() {
        imageUri?.let {
            val image = InputImage.fromFilePath(applicationContext, it)
            val recognizer = TextRecognition.getClient()
            recognizer.process(image).addOnSuccessListener { visionText ->
                binding.tvResult.text = visionText.text
            }
        }
    }

    private fun requestPermission(
            permissions: List<String>,
            permissionGranted: () -> Unit,
            permissionDenied: () -> Unit
    ) {
        Dexter.withActivity(this)
                .withPermissions(permissions)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted().not()) {
                            permissionDenied.invoke()
                            return
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            permissionDenied.invoke()
                            return
                        }

                        if (report.areAllPermissionsGranted()) {
                            permissionGranted.invoke()
                            return
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                            permissions: List<com.karumi.dexter.listener.PermissionRequest>,
                            token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener {
                    Timber.d(it.name)
                }
                .onSameThread()
                .check()
    }
}