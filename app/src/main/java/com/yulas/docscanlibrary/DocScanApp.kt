package com.yulas.docscanlibrary

import android.app.Application
import timber.log.Timber

/**
 * Created by yulas on 09/06/2021.
 */
class DocScanApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.tag(Constants.TIMBER_TAG)
        }
    }
}