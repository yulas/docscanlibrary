package com.yulas.docscannerlibrary

import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.PointF
import android.graphics.RectF
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.yulas.docscannerlibrary.databinding.FragmentScanBinding
import timber.log.Timber
import java.io.IOException
import java.util.*

/**
 * Created by yulas on 07/06/2021.
 */

class ScanFragment : Fragment() {

    private lateinit var binding: FragmentScanBinding

    private val overlayView = binding.overlayView
    private val sourceImageView = binding.ivSource
    private val sourceFrame = binding.frameSource

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_scan, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentScanBinding.inflate(layoutInflater)
        requireActivity().shortToast("Timber ScanFragment onViewCreated")
        init()
    }

    private fun init() {
        with(binding) {
            btnScan.setOnClickListener {
                val points: Map<Int, PointF> = overlayView.getPoints()
                if (isScanPointsValid(points)) {
//                    com.scanlibrary.ScanFragment.ScanAsyncTask(points).execute()
                } else {
//                    showErrorDialog()
                }
            }
        }
    }

    private fun getBitmapData(): Bitmap? {
        val uri = getUri()
        try {
            val bitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, uri)
            requireActivity().contentResolver.delete(uri!!, null, null)
            return bitmap
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun getUri(): Uri? {
        return requireArguments().getParcelable(Constants.SELECTED_BITMAP)
    }

    private fun setBitmap(original: Bitmap) {
        val scaledBitmap: Bitmap = scaledBitmap(original, sourceFrame.width, sourceFrame.height)
        sourceImageView.setImageBitmap(scaledBitmap)
        val tempBitmap = (sourceImageView.drawable as BitmapDrawable).bitmap
        val pointFs = getEdgePoints(tempBitmap)
        overlayView.setPoints(pointFs)
        overlayView.visibility = View.VISIBLE
        val padding = resources.getDimension(R.dimen.scanPadding).toInt()
        val layoutParams = FrameLayout.LayoutParams(tempBitmap.width + 2 * padding, tempBitmap.height + 2 * padding)
        layoutParams.gravity = Gravity.CENTER
        overlayView.layoutParams = layoutParams
    }

    private fun getEdgePoints(tempBitmap: Bitmap): Map<Int, PointF> {
        val pointFs = getContourEdgePoints(tempBitmap)
        return orderedValidEdgePoints(tempBitmap, pointFs)
    }

    private fun getContourEdgePoints(tempBitmap: Bitmap): List<PointF> {
        val points: FloatArray = (activity as ScanActivity).getPoints(tempBitmap)
        val x1 = points[0]
        val x2 = points[1]
        val x3 = points[2]
        val x4 = points[3]
        val y1 = points[4]
        val y2 = points[5]
        val y3 = points[6]
        val y4 = points[7]
        val pointFs: MutableList<PointF> = ArrayList()
        pointFs.add(PointF(x1, y1))
        pointFs.add(PointF(x2, y2))
        pointFs.add(PointF(x3, y3))
        pointFs.add(PointF(x4, y4))
        return pointFs
    }

    private fun getOutlinePoints(tempBitmap: Bitmap): Map<Int, PointF> {
        val outlinePoints: MutableMap<Int, PointF> = HashMap()
        outlinePoints[0] = PointF(0F, 0F)
        outlinePoints[1] = PointF(tempBitmap.width.toFloat(), 0F)
        outlinePoints[2] = PointF(0F, tempBitmap.height.toFloat())
        outlinePoints[3] = PointF(tempBitmap.width.toFloat(), tempBitmap.height.toFloat())
        return outlinePoints
    }

    private fun orderedValidEdgePoints(tempBitmap: Bitmap, pointFs: List<PointF>): Map<Int, PointF> {
        var orderedPoints: Map<Int, PointF> = overlayView.getOrderedPoints(pointFs)
        if (!overlayView.isValidShape(orderedPoints)) {
            orderedPoints = getOutlinePoints(tempBitmap)
        }
        return orderedPoints
    }

//    private class ScanButtonClickListener : View.OnClickListener {
//        override fun onClick(v: View) {
//            val points: Map<Int?, PointF?> = polygonView.getPoints()
//            if (isScanPointsValid(points)) {
//                ScanAsyncTask(points).execute()
//            } else {
//                showErrorDialog()
//            }
//        }
//    }

    private fun isScanPointsValid(points: Map<Int, PointF>): Boolean {
        return points.size == 4
    }

    private fun scaledBitmap(bitmap: Bitmap, width: Int, height: Int): Bitmap {
        val m = Matrix()
        m.setRectToRect(RectF(0F, 0F, bitmap.width.toFloat(), bitmap.height.toFloat()), RectF(0F, 0F, width.toFloat(), height.toFloat()), Matrix.ScaleToFit.CENTER)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, m, true)
    }

    private fun getScannedBitmap(original: Bitmap, points: Map<Int, PointF>): Bitmap? {
        val width = original.width
        val height = original.height
        val xRatio: Float = original.width.toFloat() / sourceImageView.width
        val yRatio: Float = original.height.toFloat() / sourceImageView.height
        val x1 = points[0]!!.x * xRatio
        val x2 = points[1]!!.x * xRatio
        val x3 = points[2]!!.x * xRatio
        val x4 = points[3]!!.x * xRatio
        val y1 = points[0]!!.y * yRatio
        val y2 = points[1]!!.y * yRatio
        val y3 = points[2]!!.y * yRatio
        val y4 = points[3]!!.y * yRatio
        Timber.d("Points($x1,$y1)($x2,$y2)($x3,$y3)($x4,$y4)")
        return (activity as ScanActivity).getScannedBitmap(original, x1, y1, x2, y2, x3, y3, x4, y4)
    }

//    private class ScanAsyncTask(private val points: Map<Int?, PointF?>) : AsyncTask<Void?, Void?, Bitmap>() {
//        override fun onPreExecute() {
//            super.onPreExecute()
//            showProgressDialog(getString(R.string.scanning))
//        }
//
//        protected override fun doInBackground(vararg params: Void): Bitmap {
//            val bitmap: Bitmap = getScannedBitmap(original, points)
//            val uri: Uri = Utils.getUri(getActivity(), bitmap)
//            scanner.onScanFinish(uri)
//            return bitmap
//        }
//
//        override fun onPostExecute(bitmap: Bitmap) {
//            super.onPostExecute(bitmap)
//            bitmap.recycle()
//            dismissDialog()
//        }
//    }
//
//    protected fun showProgressDialog(message: String?) {
//        progressDialogFragment = ProgressDialogFragment(message)
//        val fm: FragmentManager? = fragmentManager
//        progressDialogFragment.show(fm, ProgressDialogFragment::class.java.toString())
//    }
//
//    protected fun dismissDialog() {
//        progressDialogFragment.dismissAllowingStateLoss()
//    }
}