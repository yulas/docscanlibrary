package com.yulas.docscannerlibrary

import android.app.AlertDialog
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.yulas.docscannerlibrary.databinding.ActivityScanBinding
import timber.log.Timber

/**
 * Created by yulas on 07/06/2021.
 */

class ScanActivity : AppCompatActivity(), IScanner {

    private lateinit var binding: ActivityScanBinding

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        binding = ActivityScanBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        applicationContext.shortToast("Timber ScanActivity onCreate")
        Log.d("Timber", "Timber ScanActivity onCreate")
        init()
    }

    override fun onBitmapSelect(uri: Uri?) {
        Timber.d("Timber ScanActivity onBitmapSelect")
        Log.d("Timber", "Timber ScanActivity onBitmapSelect")
        applicationContext.shortToast("Timber ScanActivity onBitmapSelect")
        val fragment = ScanFragment()
        val bundle = Bundle()
        bundle.putParcelable(Constants.SELECTED_BITMAP, uri)
        fragment.arguments = bundle
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.content, fragment)
        fragmentTransaction.addToBackStack(ScanFragment::class.java.toString())
        fragmentTransaction.commit()
    }

    override fun onScanFinish(uri: Uri?) {
        Timber.d("Timber ScanActivity onScanFinish")
        Log.d("Timber", "Timber ScanActivity onScanFinish")
        applicationContext.shortToast("Timber ScanActivity onScanFinish")
        val fragment = ResultFragment()
        val bundle = Bundle()
        bundle.putParcelable(Constants.SCANNED_RESULT, uri)
        fragment.arguments = bundle
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.scan_content, fragment)
        fragmentTransaction.addToBackStack(ResultFragment::class.java.toString())
        fragmentTransaction.commit()
    }

    private fun init() {
        Timber.d("Timber ScanActivity Init")
        Log.d("Timber", "Timber ScanActivity init")
        applicationContext.shortToast("Timber ScanActivity init")
        val fragment = PickImageFragment()
        val bundle = Bundle()
        bundle.putInt(Constants.SCANNED_RESULT, getPreferenceContent())
        fragment.arguments = bundle
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.scan_content, fragment)
        fragmentTransaction.addToBackStack(PickImageFragment::class.java.toString())
        fragmentTransaction.commit()
    }

    override fun onTrimMemory(level: Int) {
        when (level) {
            TRIM_MEMORY_UI_HIDDEN -> {
            }
            TRIM_MEMORY_RUNNING_MODERATE, TRIM_MEMORY_RUNNING_LOW, TRIM_MEMORY_RUNNING_CRITICAL -> {
            }
            TRIM_MEMORY_BACKGROUND, TRIM_MEMORY_MODERATE, TRIM_MEMORY_COMPLETE ->
                AlertDialog.Builder(this)
                        .setTitle(R.string.low_memory)
                        .setMessage(R.string.low_memory_message)
                        .create()
                        .show()
            else -> {
            }
        }
    }

    private fun getPreferenceContent(): Int {
        Timber.d("Timber ScanActivity getPreferenceContent")
        Log.d("Timber", "Timber ScanActivity getPreferenceContent")
        applicationContext.shortToast("Timber ScanActivity getPreferenceContent")
        return intent.getIntExtra(Constants.OPEN_INTENT_PREFERENCE, 0)
    }

    external fun getScannedBitmap(bitmap: Bitmap?, x1: Float, y1: Float, x2: Float, y2: Float, x3: Float, y3: Float, x4: Float, y4: Float): Bitmap?

    external fun getGrayBitmap(bitmap: Bitmap?): Bitmap?

    external fun getMagicColorBitmap(bitmap: Bitmap?): Bitmap?

    external fun getBWBitmap(bitmap: Bitmap?): Bitmap?

    external fun getPoints(bitmap: Bitmap?): FloatArray
}