package com.yulas.docscannerlibrary

import android.net.Uri

/**
 * Created by yulas on 07/06/2021.
 */

interface IScanner {
    fun onBitmapSelect(uri: Uri?)

    fun onScanFinish(uri: Uri?)
}