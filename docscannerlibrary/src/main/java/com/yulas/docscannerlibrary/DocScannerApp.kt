package com.yulas.docscannerlibrary

import android.app.Application
import timber.log.Timber

/**
 * Created by yulas on 09/06/2021.
 */
class DocScannerApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}