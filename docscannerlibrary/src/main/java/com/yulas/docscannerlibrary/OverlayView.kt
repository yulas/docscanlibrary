package com.yulas.docscannerlibrary

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.content.ContextCompat
import java.util.*
import kotlin.math.abs

/**
 * Created by yulas on 07/06/2021.
 */

class OverlayView : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attributes: AttributeSet) : super(context, attributes)
    constructor(context: Context, attributes: AttributeSet, defTheme: Int) : super(context, attributes, defTheme)

    private var paint = Paint()
    private var pointer1 = getImageView(0, 0)
    private var pointer2 = getImageView(width, 0)
    private var pointer3 = getImageView(0, height)
    private var pointer4 = getImageView(width, height)
    private var midPointer13 = getImageView(0, height / 2)
    private var midPointer12 = getImageView(0, width / 2)
    private var midPointer34 = getImageView(0, height / 2)
    private var midPointer24 = getImageView(0, height / 2)
    var overlayView : OverlayView = this

    init {
        midPointer13.setOnTouchListener(MidPointTouchListenerImpl(pointer1, pointer3))
        midPointer12.setOnTouchListener(MidPointTouchListenerImpl(pointer1, pointer2))
        midPointer34.setOnTouchListener(MidPointTouchListenerImpl(pointer3, pointer4))
        midPointer24.setOnTouchListener(MidPointTouchListenerImpl(pointer2, pointer4))
        addView(pointer1)
        addView(pointer2)
        addView(midPointer13)
        addView(midPointer12)
        addView(midPointer34)
        addView(midPointer24)
        addView(pointer3)
        addView(pointer4)
        initPaint()
    }

    override fun attachViewToParent(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        super.attachViewToParent(child, index, params)
    }

    private fun initPaint() {
        paint.color = ContextCompat.getColor(context, R.color.blue)
        paint.strokeWidth = 2f
        paint.isAntiAlias = true
    }

    fun getOrderedPoints(points: List<PointF>): Map<Int, PointF> {
        val centerPoint = PointF()
        val size = points.size
        for (pointF in points) {
            centerPoint.x += pointF.x / size
            centerPoint.y += pointF.y / size
        }
        val orderedPoints: MutableMap<Int, PointF> = HashMap()
        for (pointF in points) {
            var index = -1
            if (pointF.x < centerPoint.x && pointF.y < centerPoint.y) {
                index = 0
            } else if (pointF.x > centerPoint.x && pointF.y < centerPoint.y) {
                index = 1
            } else if (pointF.x < centerPoint.x && pointF.y > centerPoint.y) {
                index = 2
            } else if (pointF.x > centerPoint.x && pointF.y > centerPoint.y) {
                index = 3
            }
            orderedPoints[index] = pointF
        }
        return orderedPoints
    }

    fun getPoints(): Map<Int, PointF> {
        val points: MutableList<PointF> = ArrayList()
        points.add(PointF(pointer1.x, pointer1.y))
        points.add(PointF(pointer2.x, pointer2.y))
        points.add(PointF(pointer3.x, pointer3.y))
        points.add(PointF(pointer4.x, pointer4.y))
        return getOrderedPoints(points)
    }

    fun setPoints(pointFMap: Map<Int, PointF>) {
        if (pointFMap.size == 4) {
            setPointsCoordinates(pointFMap)
        }
    }

    fun isValidShape(pointFMap: Map<Int, PointF>): Boolean {
            return pointFMap.size == 4
        }

    private fun setPointsCoordinates(pointFMap: Map<Int, PointF>) {
        pointer1.x = pointFMap[0]?.x ?: 0f
        pointer1.y = pointFMap[0]?.y ?: 0f
        pointer2.x = pointFMap[1]?.x ?: 0f
        pointer2.y = pointFMap[1]?.y ?: 0f
        pointer3.x = pointFMap[2]?.x ?: 0f
        pointer3.y = pointFMap[2]?.y ?: 0f
        pointer4.x = pointFMap[3]?.x ?: 0f
        pointer4.y = pointFMap[3]?.y ?: 0f
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        canvas.drawLine(
                pointer1.x + pointer1.width / 2,
                pointer1.y + pointer1.height / 2,
                pointer3.x + pointer3.width / 2,
                pointer3.y + pointer3.height / 2,
                paint
        )
        canvas.drawLine(
                pointer1.x + pointer1.width / 2,
                pointer1.y + pointer1.height / 2,
                pointer2.x + pointer2.width / 2,
                pointer2.y + pointer2.height / 2,
                paint
        )
        canvas.drawLine(
                pointer2.x + pointer2.width / 2,
                pointer2.y + pointer2.height / 2,
                pointer4.x + pointer4.width / 2,
                pointer4.y + pointer4.height / 2,
                paint
        )
        canvas.drawLine(
                pointer3.x + pointer3.width / 2,
                pointer3.y + pointer3.height / 2,
                pointer4.x + pointer4.width / 2,
                pointer4.y + pointer4.height / 2,
                paint
        )
        midPointer13.x = pointer3.x - (pointer3.x - pointer1.x) / 2
        midPointer13.y = pointer3.y - (pointer3.y - pointer1.y) / 2
        midPointer24.x = pointer4.x - (pointer4.x - pointer2.x) / 2
        midPointer24.y = pointer4.y - (pointer4.y - pointer2.y) / 2
        midPointer34.x = pointer4.x - (pointer4.x - pointer3.x) / 2
        midPointer34.y = pointer4.y - (pointer4.y - pointer3.y) / 2
        midPointer12.x = pointer2.x - (pointer2.x - pointer1.x) / 2
        midPointer12.y = pointer2.y - (pointer2.y - pointer1.y) / 2
    }

    private fun getImageView(x: Int, y: Int): ImageView {
        val imageView = ImageView(context)
        val layoutParams =
                LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        imageView.layoutParams = layoutParams
        imageView.setImageResource(R.drawable.circle)
        imageView.x = x.toFloat()
        imageView.y = y.toFloat()
        imageView.setOnTouchListener(TouchListenerImpl())
        return imageView
    }

    private inner class MidPointTouchListenerImpl(private val mainPointer1: ImageView, private val mainPointer2: ImageView) : OnTouchListener {
        var downPT = PointF() // Record Mouse Position When Pressed Down
        var startPT = PointF() // Record Start Position of 'img'

        override fun onTouch(v: View, event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_MOVE -> {
                    val mv = PointF(event.x - downPT.x, event.y - downPT.y)
                    if (abs(mainPointer1.x - mainPointer2.x) > abs(mainPointer1.y - mainPointer2.y)) {
                        if (mainPointer2.y + mv.y + v.height < overlayView.height && mainPointer2.y + mv.y > 0) {
                            v.x = (startPT.y + mv.y)
                            startPT = PointF(v.x, v.y)
                            mainPointer2.y = (mainPointer2.y + mv.y)
                        }
                        if (mainPointer1.y + mv.y + v.height < overlayView.height && mainPointer1.y + mv.y > 0) {
                            v.x = (startPT.y + mv.y)
                            startPT = PointF(v.x, v.y)
                            mainPointer1.y = (mainPointer1.y + mv.y)
                        }
                    } else {
                        if (mainPointer2.x + mv.x + v.width < overlayView.width && mainPointer2.x + mv.x > 0) {
                            v.x = (startPT.x + mv.x)
                            startPT = PointF(v.x, v.y)
                            mainPointer2.x = (mainPointer2.x + mv.x)
                        }
                        if (mainPointer1.x + mv.x + v.width < overlayView.width && mainPointer1.x + mv.x > 0) {
                            v.x = (startPT.x + mv.x)
                            startPT = PointF(v.x, v.y)
                            mainPointer1.x = (mainPointer1.x + mv.x)
                        }
                    }
                }
                MotionEvent.ACTION_DOWN -> {
                    downPT.x = event.x
                    downPT.y = event.y
                    startPT = PointF(v.x, v.y)
                }
                MotionEvent.ACTION_UP -> {
                    val color = if (isValidShape(getPoints())) {
                        ContextCompat.getColor(context, R.color.blue)
//                        resources.getColor(R.color.blue)
                    } else {
                        ContextCompat.getColor(context, R.color.orange)
//                        resources.getColor(R.color.orange)
                    }
                    paint.color = color
                }
                else -> {
                    v.performClick()
                }
            }
            overlayView.invalidate()
            return true
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return super.onTouchEvent(event)
    }

    private inner class TouchListenerImpl : OnTouchListener {
        var downPT = PointF() // Record Mouse Position When Pressed Down
        var startPT = PointF() // Record Start Position of 'img'
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_MOVE -> {
                    val mv = PointF(event.x - downPT.x, event.y - downPT.y)
                    if (startPT.x + mv.x + v.width < overlayView.width && startPT.y + mv.y + v.height < overlayView.height && startPT.x + mv.x > 0 && startPT.y + mv.y > 0) {
                        v.x = (startPT.x + mv.x)
                        v.y = (startPT.y + mv.y)
                        startPT = PointF(v.x, v.y)
                    }
                }
                MotionEvent.ACTION_DOWN -> {
                    downPT.x = event.x
                    downPT.y = event.y
                    startPT = PointF(v.x, v.y)
                }
                MotionEvent.ACTION_UP -> {
                    val color = if (isValidShape(getPoints())) {
                        ContextCompat.getColor(context, R.color.blue)
//                        resources.getColor(R.color.blue)
                    } else {
                        ContextCompat.getColor(context, R.color.orange)
//                        resources.getColor(R.color.orange)
                    }
                    paint.color = color
                }
                else -> {
                    v.performClick()
                }
            }
            overlayView.invalidate()
            return true
        }
    }
}