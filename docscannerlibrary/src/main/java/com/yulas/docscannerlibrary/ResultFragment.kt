package com.yulas.docscannerlibrary

import android.app.Activity
import android.app.FragmentManager
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yulas.docscannerlibrary.databinding.FragmentResultBinding
import java.io.IOException

/**
 * Created by yulas on 08/06/2021.
 */
class ResultFragment : Fragment() {

    private lateinit var binding: FragmentResultBinding

    private val scannedImageView = binding.scannedImage
    private val originalButton = binding.original
    private val magicColorButton = binding.magicColor
    private val grayModeButton = binding.grayMode
    private val bwButton = binding.bwMode
    private val doneButton = binding.doneButton
    private var original : Bitmap? = null
    private var transformed : Bitmap? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_scan, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentResultBinding.inflate(layoutInflater)
        initButton()
    }

    private fun initButton() {
        originalButton.setOnClickListener {
            try {
//                showProgressDialog(resources.getString(R.string.applying_filter))
                transformed = original
                scannedImageView.setImageBitmap(original)
//                dismissDialog()
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
//                dismissDialog()
            }
        }

        magicColorButton.setOnClickListener {
//            showProgressDialog(resources.getString(R.string.applying_filter))
            AsyncTask.execute {
                try {
                    transformed = (activity as ScanActivity).getMagicColorBitmap(original)
                } catch (e: OutOfMemoryError) {
                    activity?.runOnUiThread {
                        transformed = original
                        scannedImageView.setImageBitmap(original)
                        e.printStackTrace()
//                        dismissDialog()
                    }
                }
                activity?.runOnUiThread {
                    scannedImageView.setImageBitmap(transformed)
//                    dismissDialog()
                }
            }
        }

        bwButton.setOnClickListener {
//            showProgressDialog(resources.getString(R.string.applying_filter))
            AsyncTask.execute {
                try {
                    transformed = (activity as ScanActivity).getBWBitmap(original)
                } catch (e: OutOfMemoryError) {
                    activity?.runOnUiThread {
                        transformed = original
                        scannedImageView.setImageBitmap(original)
                        e.printStackTrace()
//                        dismissDialog()
                    }
                }
                activity?.runOnUiThread {
                    scannedImageView.setImageBitmap(transformed)
//                    dismissDialog()
                }
            }
        }

        grayModeButton.setOnClickListener {
//            showProgressDialog(resources.getString(R.string.applying_filter))
            AsyncTask.execute {
                try {
                    transformed = (activity as ScanActivity).getGrayBitmap(original)
                } catch (e: OutOfMemoryError) {
                    activity?.runOnUiThread {
                        transformed = original
                        scannedImageView.setImageBitmap(original)
                        e.printStackTrace()
//                        dismissDialog()
                    }
                }
                activity?.runOnUiThread {
                    scannedImageView.setImageBitmap(transformed)
//                    dismissDialog()
                }
            }
        }

        doneButton.setOnClickListener {
//            showProgressDialog(resources.getString(R.string.loading))
            AsyncTask.execute {
                try {
                    val data = Intent()
                    var bitmap: Bitmap? = transformed
                    if (bitmap == null) {
                        bitmap = original
                    }
                    val uri: Uri = getUri(requireContext(), bitmap)
                    data.putExtra(Constants.SCANNED_RESULT, uri)
                    activity?.setResult(Activity.RESULT_OK, data)
                    original?.recycle()
                    System.gc()
                    activity?.runOnUiThread {
//                        dismissDialog()
                        activity?.finish()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun getBitmap(): Bitmap? {
        val uri = getUri()
        try {
            original = MediaStore.Images.Media.getBitmap(context?.contentResolver, uri)
            requireActivity().contentResolver.delete(uri!!, null, null)
            return original
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun getUri(): Uri? {
        return requireArguments().getParcelable(Constants.SCANNED_RESULT)
    }

    fun setScannedImage(scannedImage: Bitmap?) {
        scannedImageView.setImageBitmap(scannedImage)
    }

//    @Synchronized
//    protected fun showProgressDialog(message: String?) {
//        if (ResultFragment.progressDialogFragment != null && ResultFragment.progressDialogFragment.isVisible()) {
//            // Before creating another loading dialog, close all opened loading dialogs (if any)
//            ResultFragment.progressDialogFragment.dismissAllowingStateLoss()
//        }
//        ResultFragment.progressDialogFragment = null
//        ResultFragment.progressDialogFragment = ProgressDialogFragment(message)
//        val fm: FragmentManager? = fragmentManager
//        ResultFragment.progressDialogFragment.show(fm, ProgressDialogFragment::class.java.toString())
//    }
//
//    @Synchronized
//    protected fun dismissDialog() {
//        ResultFragment.progressDialogFragment.dismissAllowingStateLoss()
//    }

}