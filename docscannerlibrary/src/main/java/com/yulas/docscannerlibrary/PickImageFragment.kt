package com.yulas.docscannerlibrary

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.AssetFileDescriptor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.yulas.docscannerlibrary.databinding.FragmentPickImageBinding
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by yulas on 09/06/2021.
 */
class PickImageFragment : Fragment() {

    private lateinit var binding: FragmentPickImageBinding
    private lateinit var scanner: IScanner

    private val cameraButton = binding.cameraButton
    private val mediaButton = binding.mediaButton
    private var fileUri: Uri? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_pick_image, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPickImageBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        Timber.d("Timber PickImageFragment init")
        cameraButton.setOnClickListener {
            openCamera()
        }
        mediaButton.setOnClickListener {
            openMediaContent()
        }
        if (isIntentPreferenceSet()) {
            handleIntentPreference()
        } else {
            requireActivity().finish()
        }
    }

    private fun clearTempImages() {
        try {
            val tempFolder = File(Constants.IMAGE_PATH)
            for (f in tempFolder.listFiles()) f.delete()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleIntentPreference() {
        val preference = getIntentPreference()
        if (preference == Constants.OPEN_CAMERA) {
            openCamera()
        } else if (preference == Constants.OPEN_MEDIA) {
            openMediaContent()
        }
    }

    private fun isIntentPreferenceSet(): Boolean {
        val preference = arguments?.getInt(Constants.OPEN_INTENT_PREFERENCE, 0)
        return preference != 0
    }

    private fun getIntentPreference(): Int {
        return arguments?.getInt(Constants.OPEN_INTENT_PREFERENCE, 0) ?: 0
    }

    private fun openMediaContent() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, Constants.PICKFILE_REQUEST_CODE)
    }

    private fun openCamera() {
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val file = createImageFile()
            val isDirectoryCreated = file.parentFile.mkdirs()
            Timber.d("openCamera: isDirectoryCreated: $isDirectoryCreated")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val tempFileUri = FileProvider.getUriForFile(requireActivity().applicationContext,
                        "com.scanlibrary.provider",  // As defined in Manifest
                        file)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri)
            } else {
                val tempFileUri = Uri.fromFile(file)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri)
            }
            startActivityForResult(cameraIntent, Constants.START_CAMERA_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA), MY_CAMERA_REQUEST_CODE)
        }
    }

    private fun createImageFile(): File {
        clearTempImages()
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val file: File = File(Constants.IMAGE_PATH, "IMG_" + timeStamp +
                ".jpg")
        fileUri = Uri.fromFile(file)
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult$resultCode")
        var bitmap: Bitmap? = null
        if (resultCode == Activity.RESULT_OK) {
            try {
                when (requestCode) {
                    Constants.START_CAMERA_REQUEST_CODE -> bitmap = getBitmap(fileUri)
                    Constants.PICKFILE_REQUEST_CODE -> bitmap = getBitmap(data?.data)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            requireActivity().finish()
        }
        bitmap?.let { postImagePick(it) }
    }

    private fun postImagePick(bitmap: Bitmap) {
        val uri: Uri = getUri(requireContext(), bitmap)
        bitmap.recycle()
        scanner.onBitmapSelect(uri)
    }

    @Throws(IOException::class)
    private fun getBitmap(selectedImg: Uri?): Bitmap? {
        val options = BitmapFactory.Options()
        options.inSampleSize = 3
        var fileDescriptor: AssetFileDescriptor? = null
        fileDescriptor = requireActivity().contentResolver.openAssetFileDescriptor(selectedImg!!, "r")
        return BitmapFactory.decodeFileDescriptor(
                fileDescriptor!!.fileDescriptor, null, options)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                Toast.makeText(activity, "camera permission denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    companion object {
        private const val MY_CAMERA_REQUEST_CODE = 100
    }

}